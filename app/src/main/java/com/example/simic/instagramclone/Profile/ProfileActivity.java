package com.example.simic.instagramclone.Profile;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.simic.instagramclone.R;
import com.example.simic.instagramclone.Utils.BottomNavigationViewHelper;
import com.example.simic.instagramclone.Utils.GridImageAdapter;
import com.example.simic.instagramclone.Utils.UniversalImageLoader;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Simic on 13.3.2018..
 */

public class ProfileActivity extends AppCompatActivity{
    
    private static final String TAG = "ProfileActivity";
    private static final int ACTIVITY_NUM = 4;
    private static final int NUM_GRID_COLUMNS = 3;
    private Context mContext = ProfileActivity.this;
    
    //@BindView(R.id.profileProgressBar) ProgressBar mProgressBar;
    //@BindView(R.id.profile_photo) ImageView profilePhoto;
    //@BindView(R.id.gridView) GridView gridView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Log.d(TAG, "onCreate: started");
        //ButterKnife.bind(this);
        //mProgressBar.setVisibility(View.GONE);
        init();


        /*
        //regionMethods
        setupBottomNavigationView();
        setSupportActionBar();
        setProfileImage();
        temGridSetup();
        //endregion
        */
    }

    private void init(){
        Log.d(TAG, "init: inflating " + getString(R.string.profile_fragment));

        ProfileFragment fragment = new ProfileFragment();

        android.support.v4.app.FragmentTransaction transaction = ProfileActivity.this.getSupportFragmentManager().beginTransaction();

        transaction.replace(R.id.container,fragment);

        transaction.addToBackStack(getString(R.string.profile_fragment));
        transaction.commit();

    }


/*
    private void temGridSetup(){
        ArrayList<String> imgURLs = new ArrayList<>();
        imgURLs.add("https://static.pexels.com/photos/132037/pexels-photo-132037.jpeg");
        imgURLs.add("https://static.pexels.com/photos/132037/pexels-photo-132037.jpeg");
        imgURLs.add("https://static.pexels.com/photos/132037/pexels-photo-132037.jpeg");
        imgURLs.add("https://static.pexels.com/photos/132037/pexels-photo-132037.jpeg");
        imgURLs.add("https://static.pexels.com/photos/132037/pexels-photo-132037.jpeg");
        imgURLs.add("https://static.pexels.com/photos/132037/pexels-photo-132037.jpeg");
        imgURLs.add("https://static.pexels.com/photos/132037/pexels-photo-132037.jpeg");
        imgURLs.add("https://static.pexels.com/photos/132037/pexels-photo-132037.jpeg");
        imgURLs.add("https://static.pexels.com/photos/132037/pexels-photo-132037.jpeg");
        imgURLs.add("https://static.pexels.com/photos/132037/pexels-photo-132037.jpeg");
        setupImageGrid(imgURLs);


    }

            private void  setupImageGrid(ArrayList<String> imgURLs){

                int gridWith = getResources().getDisplayMetrics().widthPixels;
                int imageWidth = gridWith/NUM_GRID_COLUMNS;
                gridView.setColumnWidth(imageWidth);

                GridImageAdapter adapter = new GridImageAdapter(mContext,R.layout.layout_grid_imageview,"",imgURLs);
                gridView.setAdapter(adapter);


            }

            private void setProfileImage(){
                Log.d(TAG, "setProfileImage: setting profile photo");
                String imgUrl = "https://3c1703fe8d.site.internapcdn.net/newman/gfx/news/hires/2014/android.jpg";
                UniversalImageLoader.setImage(imgUrl,profilePhoto,mProgressBar,"");

            }

            //region setupToolbar
            private void setSupportActionBar() {

                Toolbar toolbar = findViewById(R.id.profileToolBar);
                setSupportActionBar(toolbar);

                ImageView profileMenu = findViewById(R.id.profileMenu);

                profileMenu.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d(TAG, "onClick: navigation to account settings!");
                        Intent intent = new Intent(mContext,AccountSettingsActivity.class);
                        startActivity(intent);
                    }
                });


            }
            //endregion

            //region bottomNavigationSetup
            private void setupBottomNavigationView() {
                Log.d(TAG, "setupBottomNavigationView: setting up BottomNavigationView");
                BottomNavigationViewEx bottomNavigationViewEx = (BottomNavigationViewEx) findViewById(R.id.bottomNavViewBar);
                BottomNavigationViewHelper.setupBottomNavigationView(bottomNavigationViewEx);
                BottomNavigationViewHelper.enableNavigation(mContext,bottomNavigationViewEx);
                Menu menu = bottomNavigationViewEx.getMenu();
                MenuItem menuItem = menu.getItem(ACTIVITY_NUM);
                menuItem.setChecked(true);

            }
            //endregion

*/
}
