package com.example.simic.instagramclone.Utils;

/**
 * Created by Simic on 21.3.2018..
 */

public class StringManipulation {

    public static String expandUsername(String username){
        return username.replace("." , " ");
    }

    public static String condenseUsername(String username){
        return username.replace(" ", ".");
    }

}
